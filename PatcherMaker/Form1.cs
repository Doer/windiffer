﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BsDiff;


namespace PatcherMaker
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void btn_patcher_Click(object sender, EventArgs e)
        {
            this.progressBar1.Style = ProgressBarStyle.Marquee;
            
            await Task.Run(() =>
                {
                    try
                    {
                        using (FileStream output = new FileStream(this.textBox1.Text + "\\dd.apk", FileMode.Create))
                            BinaryPatchUtility.Create(File.ReadAllBytes(this.listBox1.Items[0].ToString()), File.ReadAllBytes(this.listBox1.Items[1].ToString()), output);
                    }
                    catch (FileNotFoundException ex)
                    {
                        Console.Error.WriteLine("Could not open '{0}'.", ex.FileName);
                    }
                });
          this.progressBar1.Style=ProgressBarStyle.Blocks;
        }
   
        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            this.listBox1.DataSource = files;
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = this.folderBrowserDialog1.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                this.textBox1.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;

        }
    }
}
